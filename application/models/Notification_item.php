<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 1/14/2017
 * Time: 4:44 PM
 */
class Notification_item
{

    // property declaration
    public $status;
    public $details;
    public $category_name;

    public function set_status($value){
        $this->status=$value;
    }
    public function set_details($value){
        $this->details=$value;
    }
    public function set_category_name($value){
        $this->category_name=$value;
    }

    public function get_status(){
        return $this->status;
    }
    public function get_details(){
        return $this->details;
    }
    public function get_category_name(){
        return $this->category_name;
    }

}