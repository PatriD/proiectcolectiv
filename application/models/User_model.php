<?php
Class User_model extends CI_Model
{
    function login($username, $password)
    {
        $this -> db -> select('*');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function get_group_by_user($user_id)
    {
        $this->db->select("groups.name");
        $this->db->from("groups");
        $this->db->join("users","groups.id = users.groups_id");
        $this->db->where("users.id",$user_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result()[0]->name;
        }
        return false;
    }

    function get_role_by_user($user_id)
    {
        $this->db->select("role.description");
        $this->db->from("role");
        $this->db->join("users","users.role_id = role.id");
        $this->db->where("users.id", $user_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result()[0]->description;
        }
        return false;
    }

    function get_user_data($user_id){
      $this -> db -> select('user_data_id');
      $this -> db -> from('users');
      $this -> db -> where('id', $user_id);
      $this -> db -> limit(1);
      $query = $this -> db -> get();

      if($query -> num_rows() == 1)
      {
          $user_data_id = $query->result()[0]->user_data_id;
          $this -> db -> select('*');
          $this -> db -> from('user_data');
          $this -> db -> where('id', $user_data_id);
          $this -> db -> limit(1);
          $query = $this -> db -> get();
          if($query -> num_rows() == 1)
          {
             return $query->result()[0];
          }
          else return false;

      }
      else
      {
          return false;
      }
    }

    function login_with_cookie($username, $password)
    {
        $this -> db -> select('id, username, password');
        $this -> db -> from('users');
        $this -> db -> where('username', $username);
        $this -> db -> where('password', MD5($password));
        $this -> db -> limit(1);

        $query = $this -> db -> get();

        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }

    function get_user_group($user_id)
    {
        $this -> db -> select('groups_id');
        $this -> db -> from('users');
        $this -> db -> where('id',$user_id);
        $this -> db -> limit(1);
        $query = $this -> db ->get();
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }


}
?>
