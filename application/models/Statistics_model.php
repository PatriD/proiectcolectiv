<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 1/14/2017
 * Time: 7:03 PM
 */
class Statistics_model extends CI_Model
{

    function get_flow_id_by_document_id($document_id){
        $this -> db -> select('flow_id');
        $this -> db -> from('step_status');
        $this -> db -> where('document_id', $document_id);
        $query = $this -> db -> get();
        return $query->result();
    }

    function get_documents_by_category_id($category_id){
        $this -> db -> select('*');
        $this -> db -> from('document');
        $this -> db -> where('category_id', $category_id);
        $query = $this -> db -> get();
        return $query->result();
    }

    function get_logs_by_document_id($document_id){
        $this -> db -> select('*');
        $this -> db -> from('log');
        $this -> db -> where('document_id', $document_id);
        $query = $this -> db -> get();
        return $query->result();
    }

    function get_logs_by_category_id($category_id){
        $documents = $this->get_documents_by_category_id($category_id);
        $arr = array();
        foreach($documents as $doc){
            $logs = $this->get_logs_by_document_id($doc->id);
            foreach($logs as $log){
                array_push($arr,$log);
            }
        }
        return $arr;
    }

    function get_logs_created_current_week($logs){

        $start_date = gmDate("Y-m-d");
        $date = DateTime::createFromFormat('Y-m-d',$start_date);

        $date->modify('-7 day');

        $arr = array();
        foreach($logs as $log){
            if(DateTime::createFromFormat('Y-m-d',$log->created_on) >= $date) {
                array_push($arr, $log);
            }
        }
        return $arr;
    }
    function get_logs_completed_current_week($logs){

        $start_date = gmDate("Y-m-d");
        $date = DateTime::createFromFormat('Y-m-d',$start_date);

        $date->modify('-7 day');
//        echo $date->format('Y-m-d');//2013-04-06

        $arr = array();
        foreach($logs as $log){
            if(DateTime::createFromFormat('Y-m-d',$log->completed_on) >= $date) {
                array_push($arr, $log);
            }
        }
        return $arr;
    }


    function get_documents_with_flow_by_category_id($category_id){
        $documents = $this->get_documents_by_category_id($category_id);
        $doc_array = array();
        $i=0;
        foreach($documents as $document){
            if(count($this->get_flow_id_by_document_id($document->id)>0)){
                $doc_array[$i]=$document;
                $i+=1;
            }
        }
    }





}