<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 12/5/2016
 * Time: 10:24 PM
 */
include("Notification_item.php");

class Notification_model extends CI_Model
{

    function add_notification_group($group_id,$document_id,$status,$details){

        $this -> db -> select('id');
        $this -> db -> from('users');
        $this -> db -> where('groups_id', $group_id);
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            $result=$query->result();
        }

        foreach($result as $user_id){
            $this->add_notification($user_id->id,$document_id,$status,$details);
        }

    }

  

    function delete_notifications_by_stuff($status,$document_id){
        $this -> db -> delete();
        $this -> db -> from('notification');
        $this -> db -> where('status', $status);
        $this -> db -> where('document_id', $document_id);
        $query = $this -> db -> get();
        if($query -> num_rows() > 0) {
            $result=$query->result();
        }
    return $result;
    }

    function add_notification($user_id,$document_id,$status,$details)
    {
        $data = array(
            "user_id" => $user_id,
            "document_id" => $document_id,
            "status" =>  $status,
            "details" => $details,
            "created_on" => $date = date('Y-m-d H:i:s')
            );
        $this->db->insert('notification',$data);
        if($this->db->affected_rows()>0)
            return $this->db->insert_id();
        else
            return false;
    }


    function get_category_by_document_id($document_id){
        $this -> db -> select('category_id');
        $this -> db -> from('document');
        $this -> db -> where('id', $document_id);
        $query = $this -> db -> get();
        $category_id = $query->result()[0]->category_id;

        $this -> db -> select('name');
        $this -> db -> from('category');
        $this -> db -> where('id', $category_id);
        $query2 = $this -> db -> get();

        return $query2->result();
    }


    function get_notifications_by_user_id($user_id){
        $this -> db -> select('*');
        $this -> db -> from('notification');
        $this -> db -> where('user_id', $user_id);
        $this -> db -> order_by('created_on','DESC');
        $this -> db -> limit(5);
        $query = $this -> db -> get();

        $arr = array();
        if($query -> num_rows() > 0) {
//            return $query->result

            $i = 0;
        foreach($query->result() as $notif){
            $category_name = $this->get_category_by_document_id($notif->document_id)[0]->name;
            $status = $notif->status;
            $details = $notif->details;

            $notifItem = new Notification_item();
            $notifItem->category_name=$category_name;
            $notifItem->details=$details;
            $notifItem->status=$status;

            $arr[$i]=$notifItem;
            $i+=1;

        }

        }
        return $arr;

    }



    function update_status($id,$status)
    {
        $data = array(
            "status" => $status
        );
        $this->db->where('id',$id);
        $this->db->update('notification',$data);
        return $this->db->affected_rows() > 0;
    }



}
