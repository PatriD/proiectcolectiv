<?php
class Document_model extends CI_Model
{
    function add_category($name)
    {
        $data = array(
            "name" => $name
        );
        $this->db->insert('category',$data);
        if($this->db->affected_rows()>0)
            return $this->db->insert_id();
        else
            return false;
    }



    function add_flow($document_id, $steps)
    {
        $this->db->select("category.id");
        $this->db->from("document");
        $this->db->join("category","category.id = document.category_id");
        $this->db->where("document.id",$document_id);
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            $category_id = $query->result()[0];
            $this->db->insert("flow",array("category_id" => $category_id));
            if($this->db->affected_rows() > 0 )
            {
                $flow_id = $this->db->insert_id();
                for($i=0 ; $i < count($steps) ; $i++ )
                {
                    add_step($flow_id,$i+1,$steps[$i]);
                }
            }
        }
    }

    function add_flow2($document_id, $steps)
    {

            $category_id = $document_id;
            $this->db->insert("flow",array("category_id" => $category_id));
            if($this->db->affected_rows() > 0 )
            {
                $flow_id = $this->db->insert_id();
                for($i=0 ; $i < count($steps) ; $i++ )
                {
                    $this->add_step($flow_id,$i+1,$steps[$i]);
                }
            }

    }

    function get_link_for_category($category_id)
    {
        $this->db->select("link");
        $this->db->from("category");
        $this->db->join("template","template.category_id = category.id");
        $this->db->where("category.id",$category_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0];
        }
        return false;
    }

    function add_step($flow_id, $step_position, $group_id)
    {
        $data = array(
            "flow_id" => $flow_id,
            "position" => $step_position,
            "groups_id" => $group_id
        );
        $this->db->insert('step',$data);
        return $this->db->affected_rows() > 0;
    }


    function update_category($id,$name)
    {
        $data = array(
            "name" => $name
        );
        $this->db->where('id',$id);
        $this->db->update('category',$data);
        return $this->db->affected_rows() > 0;
    }

    function add_template($id_category, $link)
    {
        $data = array(
            "link" => $link,
            "category_id" => $id_category
        );
        $this->db->insert('template',$data);
        return $this->db->affected_rows() > 0;
    }

    function update_template($id_category,$link)
    {
        $this->archive_old_input($id_category);
        $this->db->select("id");
        $this->db->where("category_id",$id_category);
        $this->db->from('template');
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            $id = $query->result()[0]->id;
        }
        $data = array(
            "link" => $link
        );
        $this->db->where("id",$id);
        $this->db->update('template',$data);
        return $this->db->affected_rows() > 0;
    }

    function new_category($category_name,$template_link)
    {
        $category_id = $this->add_category($category_name);
        if($category_id != false)
        {
            $result = $this->add_template($category_id,$template_link);
            return $category_id;
        }
        return false;
    }

    function get_categories()
    {
        $this->db->select("*");
        $this->db->from('category');
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        return false;
    }

    function get_groups()
    {
        $this->db->select("*");
        $this->db->from("groups");
        $query = $this->db->get();
        if($query->num_rows() > 0 )
        {
            return $query->result();
        }
        return false;
    }

    function get_pending_documents($user_group_id)
    {
        $this->db->select("category.name");
        $this->db->select("flow.id");
        $this->db->select("document.id as document_id, document.modified_on, document.create_on");
        $this->db->from("category");
        $this->db->join("document", "category.id = document.category_id", 'LEFT');
        $this->db->join("flow", "flow.category_id = category.id", 'LEFT');
        $this->db->join("step", "step.flow_id = flow.id", 'LEFT');
        $this->db->join("step_status", "step_status.step_id = step.id", 'LEFT');
        $this->db->where("step.groups_id", $user_group_id);
        $this->db->where("step_status.status", "pending");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function get_all_pending_documents()
    {
        $this->db->select("category.name");
        $this->db->select("flow.id");
        $this->db->select("document.id as document_id, document.modified_on, document.create_on");
        $this->db->from("category");
        $this->db->join("document", "category.id = document.category_id", 'LEFT');
        $this->db->join("flow", "flow.category_id = category.id", 'LEFT');
        $this->db->join("step", "step.flow_id = flow.id", 'LEFT');
        $this->db->join("step_status", "step_status.step_id = step.id", 'LEFT');
        $this->db->where("step_status.status", "pending");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function add_input_key_for_categ($categ_id, $input_key)
    {
        $data = array(
            "input_name" => $input_key,
            "category_id" => $categ_id
        );
        $this->db->insert('input',$data);
        return $this->db->affected_rows() > 0;
    }

    function get_entries_for_document($document_id)
    {
        $this->db->select("entry.value, input.input_name");
        $this->db->from("entry");
        $this->db->join("input","entry.input_id = input.id");
        $this->db->where("entry.document_id",$document_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function get_category_by_document($document_id)
    {
      $this->db->select("category_id");
      $this->db->from("document");
      $this->db->where("id",$document_id);
      $this->db->limit(1);
      $query = $this->db->get();
      if($query->num_rows() > 0)
      {
          return $query->result()[0]->category_id;
      }
      return false;
    }

    function get_all_documents_status()
    {
        $this->db->select("document.id,document.create_on,document.comments,document.modified_on,category.name as category_name");
        $this->db->from("document");
        $this->db->join("category","document.category_id = category.id");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $documents = $query->result();
            foreach ($documents as $document)
            {
                $this->db->select("count(step.id)");
                $this->db->from("step");
                $this->db->join("step_status","step_status.step_id = step.id");
                $this->db->where("step_status.document_id",$document->id);
                $total_steps_query = $this->db->get();
                if($total_steps_query->num_rows() > 0)
                {
                    $total_steps = $total_steps_query->result()[0]->count;
                    $document->total_number_of_steps = $total_steps;
                }

                $this->db->select("step.position");
                $this->db->from("step");
                $this->db->join("step_status","step_status.step_id = step.id");
                $this->db->where("step_status.document_id",$document->id);
                $this->db->where("step_status.status","pending");
                $this->db->limit(1);
                $current_step_query = $this->db->get();
                if($current_step_query->num_rows() > 0)
                {
                    $current_step = $current_step_query->result()[0]->position;
                    $document->current_step = $current_step;
                }else {
                    $document->current_step = $document->total_number_of_steps+1;
                }
            }
            return $documents;
        }
        return false;
    }

    function get_user_documents_status($owner_id)
   {
       $this->db->select("document.id,document.create_on,document.comments,document.modified_on,category.name as category_name");
       $this->db->where("document.owner_id",$owner_id);
       $this->db->from("document");
       $this->db->join("category","document.category_id = category.id");
       $query = $this->db->get();
       if($query->num_rows() > 0)
       {
           $documents = $query->result();
           foreach ($documents as $document)
           {
               $this->db->select("count(step.id)");
               $this->db->from("step");
               $this->db->join("step_status","step_status.step_id = step.id");
               $this->db->where("step_status.document_id",$document->id);
               $total_steps_query = $this->db->get();
               if($total_steps_query->num_rows() > 0)
               {
                   $total_steps = $total_steps_query->result()[0]->count;
                   $document->total_number_of_steps = $total_steps;
               }

               $this->db->select("step.position");
               $this->db->from("step");
               $this->db->join("step_status","step_status.step_id = step.id");
               $this->db->where("step_status.document_id",$document->id);
               $this->db->where("step_status.status","pending");
               $this->db->limit(1);
               $current_step_query = $this->db->get();
               if($current_step_query->num_rows() > 0)
               {
                   $current_step = $current_step_query->result()[0]->position;
                   $document->current_step = $current_step;
               }else {
                   $document->current_step = $document->total_number_of_steps+1;
               }
           }
           return $documents;
       }
       return false;
   }

    function get_input_id($categ_id, $input_key)
    {
        $this->db->select("id");
        $this->db->from("input");
        $this->db->where("category_id", $categ_id);
        $this->db->where("input_name", $input_key);
        $this->db->where("status","active");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
    }

    function archive_old_input($category_id)
    {
        $data = array(
            "status" => "archived"
        );
        $this->db->where("category_id",$category_id);
        $this->db->where("status","active");
        $this->db->update("input",$data);
        return $this->db->affected_rows()>0;
    }

    function get_owner_id($document_id)
    {
        $this->db->select("owner_id");
        $this->db->from("document");
        $this->db->where("document.id",$document_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result()[0]->owner_id;
        }
        return false;
    }

    function add_input_value_for_doc($doc_id, $input_id, $input_value)
    {
        $this->db->select("entry.id");
        $this->db->from("entry");
        $this->db->where("entry.input_id",$input_id);
        $this->db->where("entry.document_id",$doc_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
            return $this->update_entry($doc_id,$input_id,$input_value);
        else{
            $data = array(
                "input_id" => $input_id,
                "document_id" => $doc_id,
                "value" => $input_value
            );
            $this->db->insert('entry',$data);
            return $this->db->affected_rows() > 0;
        }
    }

    function add_document_for_category($categ_id, $user_id, $doc_type_id)
    {
        $current_date = gmDate("Y-m-d");
        $data = array(
            "owner_id" => $user_id,
            "category_id" => $categ_id,
            "type_id" => $doc_type_id,
            "modified_on" => $current_date,
            "create_on" => $current_date,
            "version" => "0.1"
        );
        $this->db->insert('document',$data);
        if( $this->db->affected_rows() > 0 )
        {
            return $this->db->insert_id();
        }
        return false;
    }

    function get_category_id_by_name($category_name)
    {
        $this->db->select("id");
        $this->db->from("category");
        $this->db->where("category.name",$category_name);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
    }

    function get_document_id($categ_id, $user_id, $doc_type_id)
    {
        $this->db->select("id");
        $this->db->from("document");
        $this->db->where("category_id", $categ_id);
        $this->db->where("owner_id", $user_id);
        $this->db->where("type_id", $doc_type_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
    }

    function get_flow_id($categ_id)
    {
        $this->db->select("id");
        $this->db->from("flow");
        $this->db->where("flow.category_id",$categ_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
    }

    function get_all_steps($flow_id)
    {
        $this->db->select("*");
        $this->db->from("step");
        $this->db->where("step.flow_id",$flow_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function add_log_for_document_id($document_id,$details)
    {
        $start_date = gmDate("Y-m-d");
//        $date = DateTime::createFromFormat('Y-m-d',$start_date);

        $data = array(
            "created_on" => $start_date,
            "document_id" => $document_id,
            "details" => $details,
            "completed_on" => null

        );
        $this->db->insert('log',$data);
        if($this->db->affected_rows()>0)
            return $this->db->insert_id();
        else
            return false;
    }

    function get_log_id($document_id)
    {
        $this->db->select("id");
        $this->db->from("log");
        $this->db->where("log.document_id",$document_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
    }

    function update_log_for_doc($document_id)
    {
        $log_id = $this->get_log_id($document_id);
        if( $log_id != false )
        {
            $current_date = gmDate("Y-m-d");
            $data = array(
                "completed_on" => $current_date
            );
            $this->db->where('id',$log_id);
            $this->db->update('log',$data);
            return $this->db->affected_rows() > 0;
        }
        return false;
    }

    function add_steps_for_doc($document_id, $categ_id)
    {
        $flow_id = $this->get_flow_id($categ_id);
        if( $flow_id != false )
        {
            $steps = $this->get_all_steps($flow_id);
            if( $steps != false )
            {
                foreach( $steps as $step )
                {
                    if( $step->position == 1 )
                    {
                        $status = "pending";
                        $data = array(
                            "flow_id" => $flow_id,
                            "document_id" => $document_id,
                            "step_id" => $step->id,
                            "status" => $status
                        );
                    }
                    else
                    {
                        $data = array(
                            "flow_id" => $flow_id,
                            "document_id" => $document_id,
                            "step_id" => $step->id
                        );
                    }
                    $this->db->insert('step_status',$data);
                }
            }
        }
    }

    function get_document_version($document_id)
    {
        $this->db->select("document.version");
        $this->db->from("document");
        $this->db->where("document.id",$document_id);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->version;
        }
        return false;
    }


    function get_category_inputs($category_id)
    {
        $this->db->select("input_name");
        $this->db->from("input");
        $this->db->where("input.category_id",$category_id);
        $this->db->where("input.status","active");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function get_document_entries($document_id)
    {
        $this->db->select("input.input_name,entry.value");
        $this->db->from("entry");
        $this->db->join("input","entry.input_id = input.id");
        $this->db->where("entry.document_id",$document_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function user_has_active_document($user_id,$category_id)
    {
        $query = $this->db->query("select document.id from document where document.owner_id = ".$user_id." and category_id = ".$category_id." and document.version LIKE'1.%'");
        return $query->num_rows() > 0;
    }

    function get_draft_id($user_id,$category_id)
    {
        $this->db->select("*");
        $this->db->from("document");
        $this->db->where("document.owner_id",$user_id);
        $this->db->like("document.version","0.");
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0]->id;
        }
        return false;
}
    function send_doc_back_to_owner($document_id, $comment)
    {
        $all_step_status = $this->get_all_step_status($document_id);
        if( $all_step_status != false )
        {
            foreach($all_step_status as $step_status)
            {
                $data = array(
                    "status" => "todo"
                );
                $this->db->where('document_id',$step_status->document_id);
                $this->db->where('flow_id',$step_status->flow_id);
                $this->db->where('step_id',$step_status->step_id);
                $this->db->update('step_status',$data);
            }
        }

        /* Add a comment for the document */
        $data = array(
            "comments" => $comment
        );
        $this->db->where('id', $document_id);
        $this->db->update('document',$data);
    }

    function advance_doc_step_status($document_id)
    {
            $cur_pending_step = $this->get_currently_pending_step($document_id);
            $next_pending_step = $this->get_next_pending_step($cur_pending_step);
            /* Change current pending step status to done */
            if( $cur_pending_step != false )
            {
                $data = array(
                    "status" => "done"
                );
                $this->db->where('document_id',$document_id);
                $this->db->where('step_id',$cur_pending_step->id);
                $this->db->update('step_status',$data);
            }

            /* Change next step status to pending */
            if( $next_pending_step != false )
            {
                $data = array(
                    "status" => "pending"
                );
                $this->db->where('document_id',$document_id);
                $this->db->where('step_id',$next_pending_step->id);
                $this->db->update('step_status',$data);
            }
            else {
                /* add log because this current step was the last step */
                $this->update_log_for_doc($document_id);
            }

            /* Document was modified -> update the date */
            $current_date = gmDate("Y-m-d");
            $data = array(
                "modified_on" => $current_date
            );
            $this->db->where('id',$document_id);
            $this->db->update('document',$data);
    }

    function get_currently_pending_step($document_id)
    {
        $this->db->select("*");
        $this->db->from("step");
        $this->db->join("step_status","step.id = step_status.step_id");
        $this->db->where("step_status.document_id",$document_id);
        $this->db->where("step_status.status","pending");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0];
        }
        return false;
    }

    function update_document_version($document_id,$version)
    {
        $data = array(
            "version" => $version
        );
        $this->db->where("document.id",$document_id);
        $this->db->update("document",$data);
        return $this->db->affected_rows() > 0;
    }

    function update_entry($document_id,$input_id,$value)
    {
        $data = array(
            "value" => $value
        );
        $this->db->where("entry.document_id",$document_id);
        $this->db->where("input_id",$input_id);
        $this->db->update("entry",$data);
        if($this->db->affected_rows() > 0)
            return true;
        else {
            $data = array(
                "value" => $value,
                "input_id" => $input_id,
                "document_id" => $document_id
            );
            $this->db->insert("entry",$data);
            return true;
        }
    }

    function get_next_pending_step($cur_pending_step)
    {
        $next_position = $cur_pending_step->position + 1;
        $this->db->select("*");
        $this->db->from("step");
        $this->db->where("step.flow_id",$cur_pending_step->flow_id);
        $this->db->where("step.position",$next_position);
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result()[0];
        }
        return false;
    }

    function get_all_step_status($document_id)
    {
        $this->db->select("*");
        $this->db->from("step_status");
        $this->db->where("step_status.document_id", $document_id);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            return $query->result();
        }
        return false;
    }

    function add_annex_for_document($document_id,$annex_link)
    {
        $data = array(
                "link" => $annex_link,
                "document_id" => $document_id
        );
        $this->db->insert("annex",$data);
        return $this->db->affected_rows() > 0;
    }

    function get_annexes_for_document($document_id)
    {
        $this->db->select("link");
        $this->db->from("annex");
        $this->db->where("document_id",$document_id);
        $query = $this->db->get();
        if($query->num_rows()>0)
        {
            return $query->result();
        }
        return false;
    }

    function remove_draft($draft_id)
    {
        $this->db->delete('log', array('document_id' => $draft_id));
        $this->db->delete('notification', array('document_id' => $draft_id));
        $this->db->delete('entry', array('document_id' => $draft_id));
        $this->db->delete('step_status', array('document_id' => $draft_id));
        $this->db->delete('document', array('id' => $draft_id));
        return $this->db->affected_rows() > 0;
    }
}
?>
