<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Pendingflows extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }

        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index(){
        $data = [];
        $group_id = $this->User_model->get_user_group($this->session->userdata('logged_in')['id'])[0]->groups_id;
        $user_role = $this->User_model->get_role_by_user($this->session->userdata('logged_in')['id']);
        if( $user_role == "admin" )
        {
            $data['flows'] = $this->Document_model->get_all_pending_documents();
        }
        else {
            $data['flows'] = $this->Document_model->get_pending_documents($group_id);
        }
        $user_id = $this->session->userdata('logged_in')['id'];
        $data['Document_model'] = $this->Document_model;
      	$this->load->view('pendingflows',$data);
    }

    public function accept($id){
        //TODO: sign document! + metadata perhaps
        //todo: notif to current group that document has been signed
        $group_id = $this->Document_model->get_currently_pending_step($id)->groups_id;
        $status = 'accepted';
        $details = 'This document has been signed.';
        $this->Notification_model->add_notification_group($group_id,$id,$status,$details);
        $this->Document_model->advance_doc_step_status($id);
        //todo: add notif to next group
        //get group id with same func.
        //status "pending" det: doc needs to be signed
        $pending_step = $this->Document_model->get_currently_pending_step($id);
        if($pending_step!=false){
            $group_id = $pending_step->groups_id;
        }
        $status = 'pending';
        $details = 'This document needs assessment.';
        $this->Notification_model->add_notification_group($group_id,$id,$status,$details);
        //notif to owner that doc has been signed
        $status = 'accepted';
        $details = 'This document has been signed.';
        $owner_id = $this->Document_model->get_owner_id($id);
        $this->Notification_model->add_notification($owner_id,$id,$status,$details);
        $this->index();
    }

    public function refuse($id,$reason){
      $this->Document_model->send_doc_back_to_owner($id,urldecode($reason));
      $this->index();
    }

}
