<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model','',true);
        $this->load->library('session');
        $this->load->helper('url');
    }

    function index()
    {
       //This method will have the credentials validation
       $this->load->library('form_validation');
       $this->form_validation->set_rules('username', 'Username', 'trim|required');
       $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if($this->form_validation->run() == false)
        {
            //Field validation failed.  User redirected to login page
            $this->load->view('login');
        }
        else
        {
            //Go to private area
           redirect('home', 'refresh');
        }
    }

    function check_database($password)
    {
       //Field validation succeeded.  Validate against database
       $username = $this->input->post('username');
       $remember_me = $this->input->post('remember_me');

       //query the database
       $result = $this->User_model->login($username, $password);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array(
                    'id' => $row->id,
                    'username' => $row->username,
                    'role' => $row->role_id
                    );
                $this->session->set_userdata('logged_in', $sess_array);
                if($remember_me != NULL)
                {
                    setcookie('id', $row->id, time() + (86400*30),"/");
                    setcookie('username', $username, time() + (86400*30),"/");
                    setcookie('password', MD5($password), time() + (86400*30),"/");
                }
            }
            return true;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}
