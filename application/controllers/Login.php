<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('User_model','',true);
    }

    function index()
    {
    	if(isset($_COOKIE['username']) && isset($_COOKIE['password']))
        {
    		if($this->cookie_login($_COOKIE['username'],$_COOKIE['password']))
            {
    			redirect('welcome', 'refresh');
    		}
    		else
            {
    			$this->load->view('login');
            }
    	}
    	else
        {
            $this->load->view('login');
        }
    }

    function cookie_login($username, $password)
    {
        $result = $this->User_model->login_with_cookie($username, $password);
        if($result){
            $sess_array = array(
                'id' => $result->id,
                'username' => $result->username,
                'role' => $result->role_id
            );
            $this->session->set_userdata('logged_in', $sess_array);
            return true;
        }
        return false;
    }
}
