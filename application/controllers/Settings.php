<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Settings extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }

        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index(){
        $data = [];
        $user_data = $this->User_model->get_user_data($this->session->userdata('logged_in')['id']);
        $data['user_data'] = $user_data;
      	$this->load->view('settings',$data);
    }

}
