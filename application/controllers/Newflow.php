<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Newflow extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }

        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function add(){

    }

    function index(){
       //$this->input->post('username');
        $data = [];
        $data['username'] = $this->session->userdata('username');
        $data['categories'] = $this->Document_model->get_categories();
        $data['groups'] = $this->Document_model->get_groups();
        $user_id = $this->session->userdata('logged_in')['id'];
        
        $arr = array();
        $categ_id = $this->input->post('category');
        if(isset($categ_id)){
          foreach ($_POST as $key => $value){
            if(strpos($key,"selected_group")!==false)
            {
              array_push($arr,$value);
            }
          }
          $this->Document_model->add_flow2($categ_id,$arr);
          $data['success']=true;
        }
      	$this->load->view('newflow',$data);
    }

}
