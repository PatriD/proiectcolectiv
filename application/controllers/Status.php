<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Status extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index(){
        $data = [];
        $user_role = $this->User_model->get_role_by_user($this->session->userdata('logged_in')['id']);
        $documents = $this->Document_model->get_user_documents_status($this->session->userdata('logged_in')['id']);
        if( $user_role == "cititor" or $user_role == "admin" )
        {
            $documents = $this->Document_model->get_all_documents_status();
        }
        $data["documents"] = $documents;
        $data['Document_model'] = $this->Document_model;
      	$this->load->view('status',$data);
    }

    public function show_completed_pdf($doc_id){
        $values = $this->Document_model->get_entries_for_document($doc_id);
        $arr = [];
        foreach($values as $entry){
          $arr[$entry->input_name]=$entry->value;
        }
        //var_dump($this->Document_model->get_link_for_category($doc_id)->link);
        $categ_id = $this->Document_model->get_category_by_document($doc_id);
        $this->save_file($this->Document_model->get_link_for_category($categ_id)->link,$doc_id,$arr);
        $data = [];
        $data['pdf_url'] = base_url('/uploads/'.$doc_id.".pdf");
        $this->load->view('redirect',$data);
    }

}
