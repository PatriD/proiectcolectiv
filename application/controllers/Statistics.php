<?php

/**
 * Created by PhpStorm.
 * User: Teo
 * Date: 1/14/2017
 * Time: 7:43 PM
 */
class Statistics extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('Notification_model','',true);
        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->model('Statistics_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
    }

    function index(){
        $data = [];
        $group_id = $this->User_model->get_user_group($this->session->userdata('logged_in')['id'])[0]->groups_id;
        $data['flows'] = $this->Document_model->get_pending_documents($group_id);
        $user_id = $this->session->userdata('logged_in')['id'];
        $categ_id = $this->input->post('selected_category');
        $data['notifications'] = $this->Notification_model->get_notifications_by_user_id($user_id);
        $data['categories'] = $this->Document_model->get_categories();
//        $data['number_of_docs_with_flow'] = 100;



        if(isset($categ_id)){
            /* last parameter represents type id = pdf */
//            $document_id = $this->Document_model->add_document_for_category($categ_id, $user_id, 1);
            $data['number_of_docs_with_flow'] = count($this->Statistics_model->get_documents_with_flow_by_category_id($categ_id));
            $logs = $this->Statistics_model->get_logs_by_category_id($categ_id);
            $data['logs'] = $logs;
            $data['logs_created_current_week'] = count($this->Statistics_model->get_logs_created_current_week($logs));
            $data['logs_completed_current_week'] = count($this->Statistics_model->get_logs_completed_current_week($logs));
            $data['categ_id'] = $categ_id;
            foreach( $data['categories'] as $categ){
                if($categ->id == $categ_id){
                    $data['categ_name'] = $categ->name;
                }
            }

        }

        $this->load->view('statistics',$data);
    }
}