<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Startflow extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('Notification_model','',true);
        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');

    }

    function index(){
       //$this->input->post('username');
        $data = [];
        $data['categories'] = $this->Document_model->get_categories();
        $categ_id = $this->input->post('selected_category');
        $document = $this->input->post('doc');
        $draft = $this->input->post('draft');
        $annexes = explode(";",$this->input->post('annexes'));
        $user_id = $this->session->userdata('logged_in')['id'];
        $data['notifications'] = $this->Notification_model->get_notifications_by_user_id($user_id);
        $remove_draft = $this->input->post('remove_draft');

        if(isset($categ_id)){
            /* last parameter represents type id = pdf */
            $data["categ_id"]=$categ_id;
            if(!$this->Document_model->user_has_active_document($user_id,$categ_id))
            {
                // the user does not have an active document from the selected category
                if(!$this->Document_model->get_draft_id($user_id,$categ_id))
                {
                    //no draft, so new document with new entries
                    $document_id = $this->Document_model->add_document_for_category($categ_id, $user_id, 1);
                    $fields = $this->Document_model->get_category_inputs($categ_id);
                    if($document_id!=false && $document_id!=-1){
                        $this->Document_model->add_log_for_document_id($document_id,'New document');
                    }
                    if(!$fields)
                    {
                        $data["pdf_form"] = "<h1>The selected category does not have any fields<br>Please choose another category.</h1>";
                    }else {
                        $data["pdf_form"] = $this->createInputsBasedOnFields($fields,$categ_id,FALSE);
                    }

                }else{
                    //has a draft so the entries are returned
                    $document_id = $this->Document_model->get_draft_id($user_id,$categ_id);
                    $fields = $this->Document_model->get_document_entries($document_id);
                    if(!$fields)
                    {
                        $fields = $this->Document_model->get_category_inputs($categ_id);
                        if(!$fields)
                        {
                            $data["pdf_form"] = "<h1>The selected category does not have any fields<br>Please choose another category.</h1>";
                        }else {
                            $data["pdf_form"] = $this->createInputsBasedOnFields($fields,$categ_id,FALSE);
                        }
                    }else {
                        if(!$fields)
                        {
                            $data["pdf_form"] = "<h1>The selected category does not have any fields<br>Please choose another category.</h1>";
                        }else {
                            $data["pdf_form"] = $this->createInputsBasedOnFields($fields,$categ_id,TRUE);
                        }
                    }
                }
            }else{
                $data["pdf_form"] = "<h1> You already initiated a document from this category </h1>";
            }

        }
        if(isset($document)){
            if(isset($remove_draft))
            {
                $draft_id = $this->Document_model->get_draft_id($user_id,$document);
                if($draft_id)
                {
                    $this->Document_model->remove_draft($draft_id);
                }
            }
            else if(isset($draft))
            {
                //$document = categ_id
                $draft_id = $this->Document_model->get_draft_id($user_id,$document);
                $version = $this->Document_model->get_document_version($draft_id);
                $version = explode(".",$version);
                $version[1]=intval($version[1])+1;
                $this->Document_model->update_document_version($draft_id,$version[0].".".$version[1]);
                foreach ($_POST as $key => $value) {
                    if(strpos($key,"annexes")===false && strpos($key,"doc")===false && strpos($key,"draft")===false && strpos($key,"remove_draft")===false)
                    {
                        $input_id = $this->Document_model->get_input_id($document, $key);
                        $this->Document_model->update_entry($draft_id,$input_id,$value);
                    }
                }
            }else{
            // submit document
            $document_id = $this->Document_model->get_draft_id($user_id,$document);
            $this->Document_model->update_document_version($document_id,"1.1");
            foreach ($_POST as $key => $value){
                if(strpos($key,"annexes")===false && strpos($key,"doc")===false && strpos($key,"draft")===false && strpos($key,"remove_draft")===false)
                {
                    $input_id = $this->Document_model->get_input_id($document, $key);
                    $this->Document_model->add_input_value_for_doc($document_id, $input_id, $value);
                }
            }
            foreach ($annexes as $annex) {
                if(strlen($annex)>0)
                {
                    $this->Document_model->add_annex_for_document($document_id,$annex);
                }
            }
            $this->Document_model->add_steps_for_doc($document_id, $document);
                //get group id/todo:
               $group_id = $this->Document_model->get_currently_pending_step($document_id)->groups_id;
                //add notification status:"pending" details:"This document needs assessment"
                $status = 'pending';
                $details = 'This document needs assessment.';
                $this->Notification_model->add_notification_group($group_id,$document_id,$status,$details);
            }
        }

      	$this->load->view('startflow',$data);
    }

    public function show($id){
      var_dump($id);
      $this->index();
    }

}
