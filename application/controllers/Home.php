<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Home extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('User_model','',true);

        $this->load->helper('url');
    }

    function index(){
        $data = [];
        $data['username'] = $this->session->userdata('username');
        $user_id = $this->session->userdata('logged_in')['id'];
        $user_data = $this->User_model->get_user_data($user_id);
        $data['user_data'] = $user_data;


      	$this->load->view('home',$data);
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        setcookie('username', "", time() ,"/");
        setcookie('password', "", time() ,"/");
        session_destroy();
        redirect('/', 'refresh');
    }
}
