<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
    user+personal_data

*/
class Newtemplate extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        //$this->load->helper('page');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }

        $this->load->model('User_model','',true);
        $this->load->model('Document_model','',true);
        $this->load->helper('form');
        $this->load->helper('url');
        //$this->load->library('Imagick');

        //  $this->Document_model->get_categories();
    }

    function index(){
        $data = [];
        $data['username'] = $this->session->userdata('username');
        $data['categories'] = $this->Document_model->get_categories();
        $user_id = $this->session->userdata('logged_in')['id'];

      	$this->load->view('newtemplate',$data);
    }

    public function do_upload()
   {

           $hash = $this->session->userdata('username');
           $hash += $this->input->post('file');

           $config['upload_path']          = './uploads/';
           $config['allowed_types']        = 'doc|docx|pdf|xls|xlsx';
           $config['max_size']             = 2048;
           $config['file_name'] = md5(uniqid($hash, true));

           $this->load->library('upload', $config);
           $data['username'] = $this->session->userdata('username');
           $data['categories'] = $this->Document_model->get_categories();

           if ( ! $this->upload->do_upload('file'))
           {
                   $data['error'] = $this->upload->display_errors();
                   $this->load->view('newtemplate', $data);
           }
           else
           {
                  $upload_data = $this->upload->data();
                   $data['upload_data'] = $upload_data;
                   $selected_category = $this->input->post('selected_category');
                   $new_category = $this->input->post('new_category');

                   if(isset($new_category) && strlen($new_category)){
                     $categ_id = $this->Document_model->new_category($new_category,substr($upload_data['full_path'], strrpos($upload_data['full_path'], '/') + 1));
                     if( $categ_id != false )
                     {
                         $fields = $this->getFieldsFromTemplate($this->Document_model->get_link_for_category($categ_id)->link);
                         foreach( $fields as $field )
                         {
                             $this->Document_model->add_input_key_for_categ($categ_id, str_replace(' ','',$field));
                         }

                     }
                     $this->genPdfThumbnail($upload_data['full_path'],$categ_id.".jpg");
                   }
                   else if(isset($selected_category)){
                     $this->Document_model->update_template($selected_category,$upload_data['full_path']);
                     $fields = $this->getFieldsFromTemplate($this->Document_model->get_link_for_category(intval($selected_category))->link);
                     foreach( $fields as $field )
                     {

                         $this->Document_model->add_input_key_for_categ($selected_category, str_replace(' ','',$field));
                     }
                   }

                   $this->load->view('newtemplate', $data);

           }
       }

}
