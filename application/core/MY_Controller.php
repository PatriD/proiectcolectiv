<?php
include 'vendor/autoload.php';

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        if(!$this->session->userdata('logged_in')) {
            redirect('/login', 'refresh');
        }
        $this->load->model('User_model','',true);
        $this->load->helper('url');
        $this->load->model('Notification_model','',true);
        $this->load->model('Document_model','',true);
    }

  function genPdfThumbnail($source, $target)
      {

          $target = dirname($source).DIRECTORY_SEPARATOR.$target;
          //exec("$pdftk {$pdf_template_path} dump_data_fields > {$tmp}");
          exec("convert -verbose -density 150 -trim ".$source."[0] -quality 100 -flatten -sharpen 0x1.0 ".$target);
      }

    function _output($content)
    {
        $user_id = $this->session->userdata('logged_in')['id'];
        $role = $this->User_model->get_role_by_user($user_id);
        $group = $this->User_model->get_group_by_user($user_id);

        if($role=="admin"){
            $data['links'] = array("Add new template"=>"newtemplate","Add new flow"=>"newflow","Start flow"=>"startflow",
                "Pending flows"=>"pendingflows","Admin CRUD"=>"adminif", "Documents status"=>"status", "View statistics"=>"statistics");

        }
        else if($role=="contributor"){
            $data['links'] = array("Start flow"=>"startflow",
                "Pending flows"=>"pendingflows","Documents status"=>"status");

        }
        else if($role=="manager"){
            $data['links'] = array("Add new template"=>"newtemplate","Add new flow"=>"newflow","Start flow"=>"startflow",
                "Pending flows"=>"pendingflows","Documents status"=>"status", "View statistics"=>"statistics");

        }
        else if($role=="cititor"){
            $data['links'] = array("Documents status"=>"status", "View statistics"=>"statistics");
        }

        $data['content'] = &$content;
        // $data['links'] = array("Add new template"=>"newtemplate","Add new flow"=>"newflow","Start flow"=>"startflow",
        //     "Pending flows"=>"pendingflows","Admin CRUD"=>"adminif", "Documents status"=>"status", "View statistics"=>"statistics");

        $user_data = $this->User_model->get_user_data($user_id);
        $data['user_data'] = $user_data;
        $data['notifications'] = $this->Notification_model->get_notifications_by_user_id($user_id);
        $documents = $this->Document_model->get_user_documents_status($user_id);
        $data["documents"] = $documents;
        echo($this->load->view('base', $data,true));
    }


    /* short example
     echo $this->createInputsBasedOnFields($this->getFieldsFromTemplate('./uploads/form.pdf'));
     $this->insertDataIntoTemplate('./uploads/form.pdf');
    */
    function createXFDF( $file, $info, $enc='UTF-8' )
    {
        $data = '<?xml version="1.0" encoding="'.$enc.'"?>' . "\n" .
            '<xfdf xmlns="http://ns.adobe.com/xfdf/" xml:space="preserve">' . "\n" .
            '<fields>' . "\n";
        foreach( $info as $field => $val )
        {
            $data .= '<field name="' . $field . '">' . "\n";
            if( is_array( $val ) )
            {
                foreach( $val as $opt )
                    $data .= '<value>' .
                        htmlentities( $opt, ENT_COMPAT, $enc ) .
                        '</value>' . "\n";
            }
            else
            {
                $data .= '<value>' .
                    htmlentities( $val, ENT_COMPAT, $enc ) .
                    '</value>' . "\n";
            }
            $data .= '</field>' . "\n";
        }
        $data .= '</fields>' . "\n" .
            '<ids original="' . md5( $file ) . '" modified="' .
                time() . '" />' . "\n" .
            '<f href="' . $file . '" />' . "\n" .
            '</xfdf>' . "\n";
        return $data;
    }


    /* Get field names from template forms */
    function getFieldsFromTemplate($pdf_template_path)
    {
        $tmp = './uploads/testpdf.pdf';
        $pdftk = 'pdftk';
        $pdf_template_path = './uploads/'.$pdf_template_path;
        exec("$pdftk {$pdf_template_path} dump_data_fields > {$tmp}");
        $con = file_get_contents($tmp);
        preg_match_all('/(FieldName:)(.*?)(<br)/', nl2br($con), $matches);
        //echo $con . "Fsdfdsfsdfsdfds";
        $count = 0;
        $result = [];
        foreach($matches[2] as $option) {
            $result[$count] = $option;
            $count = $count + 1;
        }
        return $result;
    }

    /* Insert data into template pdf based on data from the xfdf file */
    function insertDataIntoTemplate($pdf_template_path,$params)
    {
        $path_to_xfdf_file = $this->createXFDFWithFields($pdf_template_path,$params);
        $pdf_name = substr( $path_to_xfdf_file, 0, -4 ) . 'pdf';
        $command = "pdftk $pdf_template_path fill_form $path_to_xfdf_file output $pdf_name flatten";
        exec( $command, $output, $ret );
    }

    function save_file($pdf_template_path,$new_name,$params){
      $pdf_template_path = "./uploads/".$pdf_template_path;
      $path_to_xfdf_file = $this->createXFDFWithFields($pdf_template_path,$params);
      $pdf_name = './uploads/'.$new_name . '.pdf';
      $command = "pdftk $pdf_template_path fill_form $path_to_xfdf_file output $pdf_name flatten";
      exec( $command, $output, $ret );
    }

    function createInputsBasedOnFields($listOfFields,$doc_id,$has_values)
    {
        $html = "";
        $html .= form_open('Startflow')."<input type='hidden' name='doc' value='".$doc_id."'>";
        foreach($listOfFields as $res) {
            $res2 = $res->input_name;
            $word = str_replace("_", " ", $res2);
            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $word);
            $word = ucwords($word);
            if($has_values)
            {
                $html .= "
                <label for='{$res->input_name}'>{$word}</label>
                <input type='text' name='{$res->input_name}' value='{$res->value}'/><br>
                ";
            }else{
                $html .= "
                <label for='{$res->input_name}'>{$word}</label>
                <input type='text' name='{$res->input_name}' /><br>
                ";
            }
        }
        $html .= "<input type='hidden' name='annexes' value=''>";
        $html .= "<input type='submit' class='btn btn-default' value='Submit'>";
        $html .= "<input type='submit' class='btn btn-default' value='Save draft' name='draft'>";
        $html .= "<input type='submit' class='btn btn-default' value='Remove draft' name='remove_draft'>";
        $html .= "</form>";
        return $html;
    }

    /* Create an xml (xfdf) file with field names and values for the given template form*/
    function createXFDFWithFields($pdf_template_path,$params)
    {
        $res = $this->createXFDF($pdf_template_path, $params);
        $file = './uploads/fxml.xfdf';
        file_put_contents($file, $res);
        return $file;


    function getMetadata($pdf_file_path)
    {
        // $path_temp_file = tmpfile();
        // $command = "pdftk $pdf_template_path dump_data output $path_temp_file";
        // exec($command);
        // $con = file_get_contents($path_temp_file);
        // echo $con;
        // fclose($path_temp_file);
    }

    /* Get metadata from a pdf file
     * return an associative array (key, value) = (property, value)
     */
    function getMetaDataFromTemplate($pdf_template_path)
    {
        $temp_file = tmpfile();
        $path_temp_file = stream_get_meta_data($temp_file)['uri'];

        $command = "pdftk $pdf_template_path dump_data output $path_temp_file";
        exec($command);
        $con = file_get_contents($path_temp_file);
        $matches = explode(PHP_EOL, $con);

        $result = array();
        $i = 1;

        while($i < count($matches))
        {

            $optionKey = $matches[$i];

            preg_match_all('/(?<=InfoKey:)(.*)/', $optionKey, $matchKey);    //InfoKey: key -> matches = key
            if(!empty($matchKey[0]))
            {
                $optionValue = $matches[$i+1];
                preg_match_all('/(?<=InfoValue:)(.*)/', $optionValue, $matchValue);
                if(!empty($matchValue[0]))
                {
                    $result[$matchKey[0][0]] = $matchValue[0][0];
                }
                $i = $i + 2;
            }
            else {
                $i = $i + 1;
            }
        }

        fclose($temp_file);
        return $result;
    }

    function setMetaDataToTemplate($pdf_template_path, $key, $value)
    {
        $temp_file_read = tmpfile();
        $temp_file_write = tmpfile();

        /* Read metadata into temporary file */
        $path_temp_file = stream_get_meta_data($temp_file_read)['uri'];

        $command = "pdftk $pdf_template_path dump_data output $path_temp_file";
        exec($command);
        $con = file_get_contents($path_temp_file);
        $matches = explode(PHP_EOL, $con);

        /* Insert given key and value into metadata string */
        $con = $con."\n"."InfoBegin\nInfoKey: ".$key."\nInfoValue: ".$value;

        /* Write new metadata into temporary file */
        fwrite($temp_file_write, $con);

        /* Execute command to write string to pdf file */
        $command = "pdftk $pdf_template_path update_info $temp_file_write output $pdf_template_path_new";
        exec($command);

        fclose($temp_file_read);
        fclose($temp_file_write);
    }
}

}

?>
