<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login Form</title>
      <link rel="stylesheet" href="<?php echo '/proiectcolectiv/assets/css/login.css'; ?>">
</head>

<body>
  <body>
	<div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Login</h1>
			</div>

			  <?php
          $error_message = validation_errors();
          if ("" != $error_message)
          {
              echo '<div class="alert alert-danger">';
              echo $error_message;
              echo '</div>';
          }
        ?>
			<div class="login-form">
			  <?php echo form_open('verifylogin'); ?>
          <div class="control-group">
  				      <input type="text" class="login-field" value="" placeholder="username" id="username" name="username">
  				      <label class="login-field-icon fui-user" for="login-name"></label>
  				</div>

  				<div class="control-group">
        				<input type="password" class="login-field" value="" placeholder="password" id="password" name="password">
        				<label class="login-field-icon fui-lock" for="login-pass"></label>
  				</div>
          <input type="checkbox" class="login-inline-info-left login-remember" name="remember_me" value="Remember">
              <div class=login-remember-text>
                  Remember
              </div>
          </input>
        				<input type="submit" class="btn btn-primary btn-large btn-block" value="Login"/>
        				<a class="login-link" href="#" onclick="alert('Too bad')">Lost your password?</a>
				</form>
			</div>
		</div>
	</div>
</body>


</body>
</html>
