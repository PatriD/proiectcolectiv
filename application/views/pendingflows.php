<div class="header">
                  <h1 class="page-header">
                      Pending Flows <small>Shows you all the documents for which an action is required from your part</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">Pending Flows</li>
    </ol>

</div>
<script>
function refuse(id,base_url){
  window.location.replace(base_url+"/"+encodeURIComponent(document.getElementById("refuse_comment"+id).value));
}
</script>
<div class="row">
  <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
         Pending flows
        </div>
              <div class="panel-body">
              <label>Documents you can sign</label>
              <?php if($flows!=false):?>
              <?php foreach($flows as $flow): ?>

              <div class="panel panel-default">
               <div class="panel-heading">
                <?php echo $flow->name;?> - <?php echo $flow->document_id;?>
               </div>
                     <div class="panel-body">
                       <label><?php echo 30-intval((strtotime("now")-strtotime($flow->modified_on))/60/60/24);?> days left to take an action</label>
                       <br>
                     <a target="_blank" href="<?php echo base_url('index.php/status/show_completed_pdf/'.$flow->document_id);?>">
                       <button type="button" class="btn btn-default">View Document</button></a>

                       <a href="<?php echo base_url('index.php/pendingflows/accept/'.$flow->document_id);?>"><button type="button" class="btn btn-success">Accept</button></a>
                       <button id="<?php echo $flow->document_id;?>" type="button" class="btn btn-warning" onclick="refuse(this.id,'<?php echo base_url('index.php/pendingflows/refuse/'.$flow->document_id);?>')">Refuse</button>
                       <input type="text" id="<?php echo 'refuse_comment'.$flow->document_id;?>" name="refuse_comment" placeholder="Refuse reason">
                    <p>
                    <label>Annexes</label>
                    <br>
                    <?php $annexes = $Document_model->get_annexes_for_document($flow->document_id); ?>
                    <?php if($annexes!=false) foreach($annexes as $annex){
                      echo '<a href="'.$annex->link.'">'.$annex->link."</a><br>";
                    } else echo "No documentes annexed";?>
                     </div>
               </div>
               <?php endforeach;?>
             <?php endif;?>
              </div>
        </div>

        <?php
          if(isset($pdf_form)){
            ?>
             <div class="panel panel-default">
              <div class="panel-heading">
               Document
              </div>
                    <div class="panel-body">
                    <?php echo $pdf_form;?>
                    </div>
              </div>
            <?php
          }
        ?>

      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
