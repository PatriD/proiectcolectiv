<div class="header">
                  <h1 class="page-header">
                      Settings <small>View your account details</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">Settings</li>
    </ol>

</div>
<div class="row">
  <div class="col-lg-12">



      <div class="panel panel-default">
        <div class="panel-heading">
         Info
        </div>
              <div class="panel-body">
              <?php if(isset($user_data)): ?>
                <label>Name: <?php echo $user_data->name;?></label>
                <br>
                <label>Surname: <?php echo $user_data->surname;?></label>
                <br>
                <label>Phone Number: <?php echo $user_data->phone;?></label>
                <br>
                <label>Email: <a href="mailto:<?php echo $user_data->email;?>"><?php echo $user_data->email;?></a></label>
              <?php endif;?>
              </div>
        </div>

        <?php
          if(isset($pdf_form)){
            ?>
             <div class="panel panel-default">
              <div class="panel-heading">
               Document
              </div>
                    <div class="panel-body">
                    <?php echo $pdf_form;?>
                    </div>
              </div>
            <?php
          }
        ?>

      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
