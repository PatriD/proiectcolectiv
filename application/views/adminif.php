<!DOCTYPE html>
<html>
    <head>
	<meta charset="utf-8" />
        <?php foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php endforeach; ?>
        <?php foreach($js_files as $file): ?>
	    <script src="<?php echo $file; ?>"></script>
        <?php endforeach; ?>
        <style type='text/css'>
body
{
        font-family: Arial;
        font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
        text-decoration: underline;
}
        </style>
        <script type="text/javascript">
            function goTo() {
                var sE = null, url;
                if(document.getElementById) {
                    sE = document.getElementById('tableUrl');
                } else if(document.all) {
                    sE = document.all['urlList'];
                }
                if(sE && (url = sE.options[sE.selectedIndex].value)) {
                    location.href = url;
                }
            }
        </script>
    </head>
    <body>
      <div class="header">
                        <h1 class="page-header">
                            Admin CRUD <small>Direct access for all the tables in the database</small>
                        </h1>
            <ol class="breadcrumb">
            <li><a href="<?php echo base_url('home')?>">Home</a></li>
            <li><a href="<?php echo site_url('adminif/index')?>">CRUD Index</a></li>
            <li class="active">Admin CRUD</li>
          </ol>

      </div>
      <div class="row">
        <div class="col-lg-12">


<?php if (!empty($tables)): ?>
            <div class="panel panel-default">
              <div class="panel-heading">
               Admin CRUD
              </div>
                    <div class="panel-body">


                        <select id="tableUrl" data-placeholder="Choose a table..." class="form-control" style="width:350px;" tabindex="2" onchange="goTo();">
                            <option value=""></option>

                        <?php foreach ($tables as $table): ?>
                            <option value="<?php echo site_url("adminif/$table")?>"><?php echo $table?></option>
                        <?php endforeach; ?>
                        </select>
        <!--                 <div class="chzn-container chzn-container-single">
                            <input class="chzn-drop" type="button" value="Go" onclick="goTo();">
                        </div> -->

                    </div>
              </div>
  <?php endif; ?>

            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
      </div>


        <div style='height:20px;'>
        </div>

        <div>
            <?php echo $output; ?>
        </div>
    </body>
</html>
