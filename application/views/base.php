﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Misbits - Proiect Colectiv</title>
    <!-- Bootstrap Styles-->
    <link href="/proiectcolectiv/assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="/proiectcolectiv/assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="/proiectcolectiv/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="/proiectcolectiv/assets/css/custom-styles.css" rel="stylesheet" />


    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/proiectcolectiv/assets/css/progress-wizard.min.css" rel="stylesheet">
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="/proiectcolectiv/assets/js/Lightweight-Chart/cssCharts.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url("index.php/home");?>"><strong>misbits</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <?php $count = 0;?>
                        <?php if($documents): foreach($documents as $doc):?>
                          <?php if($doc->total_number_of_steps==0) continue;?>
                          <?php if($count>4) {continue; $count++;}?>
                          <?php $percentage = ($doc->current_step-1)*100/$doc->total_number_of_steps;?>
                          <li>
                            <a href="<?php echo base_url('index.php/status');?>">
                                <div>
                                    <p>
                                        <strong><?php echo $doc->category_name;?></strong>
                                        <span class="pull-right text-muted"><?php echo number_format($percentage,1);?>% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $percentage;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage;?>%">
                                            <span class="sr-only"><?php echo $percentage;?>% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                      <?php endforeach;?>
                  <?php else: echo "You have no initiated documents.";?>
                  <?php endif;?>
                        <li>
                            <a class="text-center" href="<?php echo base_url('index.php/status');?>">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">

                        <?php

                        foreach($notifications as $notification){
                            $link = base_url('pendingflows');
                            echo'<li>
                                    <a href="pendingflows">
                                    <div>
                                    <i class="fa fa-comment fa-fw"></i>';
                            echo $notification->details.' On document: '.$notification->category_name.' with status: '.$notification->status;
                            echo '</div>
                                   </a>
                                    </li>
                                <li class="divider"></li> ';


                        }

                        ?>
<!---->
<!--                        <li>-->
<!--                            <a href="#">-->
<!--                                <div>-->
<!--                                    <i class="fa fa-comment fa-fw"></i> --><?php
//                                    echo count($notifications);
//                                    if(sizeof($notifications)>=1) {
//                                        echo $notifications[0]->details;
//                                        echo '  ,document:';
//                                        echo $notifications[0]->category_name;
//                                    }
//                                    ?>
<!---->
<!--                                </div>-->
<!--                            </a>-->
<!--                        </li>-->
<!--                        <li class="divider"></li>-->

                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><i class="fa fa-user fa-fw"></i> <?php echo  $this->session->userdata('logged_in')['username'];?>
                        </li>


                        <li><a href="<?php echo base_url('index.php/settings');?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="home/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
		<div id="sideNav" href=""><i class="fa fa-caret-right"></i></div>
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <?php foreach ($links as $title => $url): ?>
                      <li>
                          <a href="<?php echo site_url(htmlentities($url)); ?>"><i class="fa fa-dashboard"></i><?php echo htmlentities($title); ?></a>
                      </li>
                    <?php endforeach; ?>

                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->

		<div id="page-wrapper">
      <?php echo $content;?>
    </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="/proiectcolectiv/assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="/proiectcolectiv/assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Js -->
    <script src="/proiectcolectiv/assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="/proiectcolectiv/assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="/proiectcolectiv/assets/js/morris/morris.js"></script>


	<script src="/proiectcolectiv/assets/js/easypiechart.js"></script>
	<script src="/proiectcolectiv/assets/js/easypiechart-data.js"></script>

	 <script src="/proiectcolectiv/assets/js/Lightweight-Chart/jquery.chart.js"></script>

    <!-- Custom Js -->
    <script src="/proiectcolectiv/assets/js/custom-scripts.js"></script>

      <script>

      </script>

</body>

</html>
