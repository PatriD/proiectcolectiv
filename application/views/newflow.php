<script>
$( document ).ready(function() {


    console.log( "ready!" );
    $('#addstep').on('click', function() {
      var div = document.getElementById('steps');
      div.innerHTML = div.innerHTML + '<input type="hidden" name="category" value="'+$("#select_category").val()+'">'
      console.log("click");
      var div = document.getElementById('steps');
      var steps_count = parseInt($("#steps_number").val());
      for(var i=1;i<=steps_count;i++)
      div.innerHTML = div.innerHTML + '  <div class="panel panel-default"> \
          <div class="panel-heading"> \
           Step '+i+' \
          </div> \
                <div class="panel-body"> \
                <input type="hidden" name="index" value="'+i+'"> \
                <?php if($groups==false) echo "There are no user groups"; else{?> \
                <select id="select_group" class="form-control" name="selected_group'+i+'"> \
                    <?php foreach($groups as $group): ?> \
                       <option value="<?php echo $group->id;?>"><?php echo $group->name;?></option> \
                    <?php endforeach;?> \
                </select> \
                <?php } ?> \
                </div> \
          </div>';
    });
});
</script>
<div class="header">
                  <h1 class="page-header">
                      New flow <small>Define a new flow for a specific document</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">New Flow</li>
    </ol>

</div>
<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <?php if(isset($success)&&$success==true) echo "Flow added successfuly!"; ?>
       New flow
      </div>
            <div class="panel-body">
              <div class="form-group">
                  <label>Select type of document</label>
                  <select id="select_category" class="form-control" name="selected_category">
                      <?php foreach($categories as $categ): ?>
                         <option value="<?php echo $categ->id;?>"><?php echo $categ->name;?></option>
                      <?php endforeach;?>
                  </select>
                  <label>Select number of steps for flow to follow:</label>
                  <input id="steps_number" type="text">
                  <button id="addstep" class="btn btn-default" type="button">Click Me!</button>
              </div>
            </div>
      </div>

      <?php echo form_open('Newflow'); ?>
      <div id="steps">
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
         Submit
        </div>
              <div class="panel-body">
              <input type="submit" class="btn btn-default" value="Submit">
              </div>
        </div>
    </form>
      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
