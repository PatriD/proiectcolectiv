<script>
$( document ).ready(function() {
    console.log( "ready!" );
    $('#new_categ').on('input', function() {
        if($('#new_categ').val().length>0)
            $('#select_category').prop('disabled', true);
        else {
          $('#select_category').prop('disabled', false);
        }
    });
});
</script>
<div class="header">
                  <h1 class="page-header">
                      New Template <small>Upload a PDF to be used in a flow</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">New Template</li>
    </ol>

</div>
<div class="row">
  <div class="col-lg-12">
      <div class="panel panel-default">
          <div class="panel-heading">
              Add new template
          </div>
          <div class="panel-body">
              <div class="row">
                  <div class="col-lg-10">
                    <?php if(isset($error)){?>
                      <div class="alert alert-error">
                        <strong>Error!</strong> <?php echo $error;?>
                      </div>
                    <?php }?>
                    <?php if(isset($upload_data)){?>
                      <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $upload_data['file_name'];?> uploaded successfuly!
                      </div>
                    <?php }?>
                    <?php if(isset($test)){?>
                      <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $test;?>
                      </div>
                    <?php }?>
                      <?php echo form_open_multipart('newtemplate/do_upload');?>

                          <div class="form-group">
                              <label>Select existent category</label>

                              <select id="select_category" class="form-control" name="selected_category">
                                  <?php foreach($categories as $categ): ?>
                                     <option value="<?php echo $categ->id;?>"><?php echo $categ->name;?></option>
                                  <?php endforeach;?>
                              </select>
                          </div>
                          <div class="form-group">
                              <label>Document template category</label>
                              <input id="new_categ" name="new_category" class="form-control" placeholder="New category if not existent">
                          </div>
                          <div class="form-group">
                              <label>File input</label>
                              <input name="file" type="file">
                          </div>

                          <button type="submit" class="btn btn-default">Upload</button>
                      </form>
                  </div>

                  <!-- /.col-lg-6 (nested) -->
              </div>
              <!-- /.row (nested) -->
          </div>
          <!-- /.panel-body -->
      </div>


      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
