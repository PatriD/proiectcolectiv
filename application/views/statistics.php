
<div class="header">
                  <h1 class="page-header">
Statistics
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">Statistics</li>
    </ol>

</div>
<div class="row">
  <div class="col-lg-12">



      <div class="panel panel-default">
        <div class="panel-heading">
Fantastic Statistics
</div>
              <div class="panel-body">


              </div>
          <div class="panel-body">
              <label>Select type of document</label>
              <?php echo form_open('statistics'); ?>
              <select id="select_category" class="form-control" name="selected_category">
                  <?php foreach($categories as $categ): ?>

                      <?php

                      if(isset($categ_id)){
                          if($categ_id==$categ->id){
                              echo '<option selected value="'.$categ->id.'">'.$categ->name.'</option>';
                          }
                          else{
                              echo '<option value="'.$categ->id.'">'.$categ->name.'</option>';
                          }
                      }else{
                          echo '<option value="'.$categ->id.'">'.$categ->name.'</option>';
                      }

                      ?>

<!--                      <option value="--><?php //echo $categ->id;?><!--">--><?php //echo $categ->name;?><!--</option>-->
                  <?php endforeach;?>

                  ?>
              </select>
              <input type="submit" class="btn btn-default" value="Submit">
              </form>

              <label>
                      <?php
                      if(isset($number_of_docs_with_flow)) {
                          echo 'Number of documents included in flows:    ' . ($number_of_docs_with_flow);
                          echo '<br>';
                          echo 'Number of documents created in the last 7 days :   '.($logs_created_current_week);
                          echo '<br>';
                          echo 'Number of documents completed in the last 7 days :   '.($logs_completed_current_week);
                      }
                      ?>
              </label>
          </div>
      </div>


</div>
</div>



<!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Template by: <a href="http://webthemez.com">WebThemez</a></p></footer>
