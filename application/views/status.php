<style>


/* A totally custom override */

.progress-indicator.custom-complex {
    background-color: #f1f1f1;
    padding: 10px 5px;
    border: 1px solid #ddd;
    border-radius: 10px;
}
.progress-indicator.custom-complex > li .bubble {
    height: 12px;
    width: 99%;
    border-radius: 2px;
    box-shadow: inset -5px 0 12px rgba(0, 0, 0, 0.2);
}
.progress-indicator.custom-complex > li .bubble:before,
.progress-indicator.custom-complex > li .bubble:after {
    display: none;
}

/* Demo for vertical bars */

.progress-indicator.stepped.stacked {
    width: 48%;
    display: inline-block;
}
.progress-indicator.stepped.stacked > li {
    height: 150px;
}
.progress-indicator.stepped.stacked > li .bubble {
    padding: 0.1em;
}
.progress-indicator.stepped.stacked > li:first-of-type .bubble {
    padding: 0.5em;
}
.progress-indicator.stepped.stacked > li:last-of-type .bubble {
    padding: 0em;
}

/* Nocenter */

.progress-indicator.nocenter.stacked > li {
    min-height: 100px;
}
.progress-indicator.nocenter.stacked > li span {
    display: block;
}

/* Demo for Timeline vertical bars */

</style>
<div class="header">
                  <h1 class="page-header">
                      Status <small>View the status of all your initiated flows</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">Status</li>
    </ol>

</div>
<div class="row">
  <div class="col-lg-12">

      <?php if(isset($documents)):?>
      <?php if($documents): foreach($documents as $doc): ?>
      <?php if($doc->total_number_of_steps==0) continue;?>
      <div class="panel panel-default">
        <div class="panel-heading">
         <?php echo $doc->category_name;?>
        </div>
              <div class="panel-body">
              <label style="float:left">Status indicator</label> <a target="_blank" href="<?php echo base_url('index.php/status/show_completed_pdf/'.$doc->id);?>" style="float:right">View Document</a>
              <p>
              <?php $refused = false;?>
              <?php if(strlen($doc->comments)>0):?>
                <?php $refused = true;?>
              <?php endif;?>
              <ul class="progress-indicator">
                  <?php if(!$refused):?>
                    <?php for($i=1;$i<=$doc->total_number_of_steps;$i++):?>
                      <?php if($i<$doc->current_step):?>
                        <li class="completed">
                            <span class="bubble"></span>
                            Step <?php echo $i;?>. <br><small>(complete)</small>
                        </li>
                      <?php endif;?>
                      <?php if($i==$doc->current_step):?>
                        <li class="active">
                            <span class="bubble"></span>
                            Step <?php echo $i;?>. <br><small>(active)</small>
                        </li>
                      <?php endif;?>
                      <?php if($i>$doc->current_step):?>
                        <li>
                            <span class="bubble"></span>
                            Step <?php echo $i;?>.
                        </li>
                      <?php endif;?>
                  <?php endfor;?>
                <?php else:?>
                  <?php for($i=1;$i<=$doc->total_number_of_steps;$i++):?>
                    <li class="refused">
                        <span class="bubble"></span>
                        Step <?php echo $i;?>. <br><small>(must resend)</small>
                    </li>
                  <?php endfor;?>
                <?php endif;?>

              </ul>
              <?php if($refused):?>
                  <label style="color:red">Document refused. Reason: <?php echo $doc->comments;?></label>
                  <br>
                  <a href="<?php echo base_url("index.php/Startflow/show/").$doc->id;?>"><button  class="btn btn-danger">Resend</button></a>
                  <p>
              <?php endif;?>

              <label style="float:left">Created on: <?php echo $doc->create_on;?></label>
              <label style="float:right">Last modified on: <?php echo $doc->modified_on;?></label>
              <br>
              <b>Annexes</b>
              <br>
              <?php $annexes = $Document_model->get_annexes_for_document($doc->id); ?>
              <?php if($annexes!=false){ foreach($annexes as $annex){
                echo '<a href="'.$annex->link.'">'.$annex->link."</a><br>";
              }} else echo "No documents annexed";?>
              </div>
        </div>
        <?php endforeach;?>
    <?php else: echo "You have no initiated documents.";?>
    <?php endif;?>
      <?php endif;?>


      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
