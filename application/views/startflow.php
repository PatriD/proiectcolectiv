<div class="header">
                  <h1 class="page-header">
                      Start Flow <small>Initiate a new flow</small>
                  </h1>
      <ol class="breadcrumb">
      <li><a href="<?php echo base_url('home')?>">Home</a></li>
      <li class="active">Start Flow</li>
    </ol>

</div>
<style>
.document_form label{
    display: inline-block;
    float: left;
    clear: left;
    width: 250px;
    text-align: right;
    padding-right: 5px;
    margin-top:10px;
}
.document_form input {
  display: inline-block;
  float: left;
  margin-top:10px;
}
</style>
<div class="row">
  <div class="col-lg-12">



      <div class="panel panel-default">
        <div class="panel-heading">
         Start flow
        </div>
              <div class="panel-body">
              <label>Select type of document</label>
              <?php echo form_open('Startflow'); ?>
                  <select id="select_category" class="form-control" name="selected_category">
                      <?php foreach($categories as $categ): ?>
                         <option value="<?php echo $categ->id;?>"><?php echo $categ->name;?></option>
                      <?php endforeach;?>
                  </select>
                  <input type="submit" class="btn btn-default" value="Submit">
              </form>
              </div>
        </div>
<script>
$( document ).ready(function() {
    $('.multi-field-wrapper').each(function() {
        var $wrapper = $('.multi-fields', this);
        $(".add-field", $(this)).click(function(e) {
            var annexes = "";
            $("input[name=stuff]")  // for all checkboxes
              .each(function() {  // first pass, create name mapping
                annexes+=this.value+";";
                $('input[name="annexes"]').val(annexes);
            });
            $('.multi-field:first-child', $wrapper).clone(true).appendTo($wrapper).find('input').val('').focus();

        });
        $('.multi-field .remove-field', $wrapper).click(function() {
            if ($('.multi-field', $wrapper).length > 1)
                $(this).parent('.multi-field').remove();
                var annexes = "";
                $("input[name=stuff]")  // for all checkboxes
                  .each(function() {  // first pass, create name mapping
                    annexes+=this.value+";";
                    $('input[name="annexes"]').val(annexes);
                });
        });
    });
});

</script>
        <?php

          if(isset($pdf_form)){
            ?>

            <div class="panel panel-default">
             <div class="panel-heading">
              Annex Documents
             </div>
                   <div class="panel-body">
                     <div class="multi-field-wrapper">
                           <div class="multi-fields">
                             <div class="multi-field">
                               <input type="text" name="stuff">
                               <button type="button" class="btn btn-danger remove-field">Remove</button>
                             </div>
                           </div>
                         <button type="button" class="btn btn-defualt add-field">Add field</button>
                       </div>
                   </div>
             </div>

             <div class="panel panel-default">
              <div class="panel-heading">
               Document
              </div>
                    <div class="panel-body">
                    <?php if(isset($categ_id)):?>
                      <a href="<?php echo base_url("/uploads/".$categ_id.".jpg");?>" target="_blank"><img style="float:right" src="<?php echo base_url("/uploads/".$categ_id.".jpg");?>" width="480"/></a>
                    <?php endif;?>
                    <div id="form" class="document_form" style="float:left">
                      
                    <?php echo $pdf_form;?>
                  </div>
                    </div>
              </div>
            <?php
          }
        ?>

      <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
<footer><p>All right reserved. Misbits UBB 2017</p></footer>
